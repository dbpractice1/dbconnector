package org.dbconnect;

import java.sql.*;

public class PostgresqlConnector {
    public static void main(String[] args) {

        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Online_Library", "postgres", "Chanu@2012");
            System.out.println("Java JDBC PostgreSQL example");

            System.out.println("Connected to Postgresql database!");
            Statement statement = connection.createStatement();
            System.out.println("Reading book records");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM public.book_list");
            while (resultSet.next()){
                System.out.println(resultSet.getString("Book_Name"));
                System.out.println(resultSet.getString("Author_Name"));
            }

        } catch (SQLException throwables) {
            System.out.println("connection failure");
            throwables.printStackTrace();
        }

    }
}
