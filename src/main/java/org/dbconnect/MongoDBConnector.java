package org.dbconnect;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;

public class MongoDBConnector {


    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoCredential credential = MongoCredential.createCredential("username","admin","password".toCharArray());
        System.out.println("Connected to database successfully");

        MongoDatabase database = mongoClient.getDatabase("admin");
        database.createCollection("sampleCollection");
        System.out.println("collection created successfully");
    }
}
